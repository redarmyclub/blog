class Comment < ActiveRecord::Base
  # 1-to-many relatioship to post
  belongs_to :post

  # validates comment's body is not empty
  validates_presence_of :body
  
  # validates post_id is not empty AND exists in the DB
 
  validates_presence_of :post_id
 
end
