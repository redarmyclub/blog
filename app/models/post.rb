class Post < ActiveRecord::Base
  # many-to-1 relationship with comments
  # comments are destroyed when their post is deleted
  has_many :comments, dependent: :destroy

  # validates of that title and body are not empty  
  validates_presence_of :title

  validates_presence_of :body

end
